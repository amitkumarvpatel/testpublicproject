//
//  ViewController.swift
//  TestPublicProject
//
//  Created by Amitkumar Patel on 10/27/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnShowAlertClicked(_ sender: Any) {
        // create the alert
        let alert = UIAlertController(title: "Title", message: "Button clicked", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        
    }
    

}

